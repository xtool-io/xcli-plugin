package xtool.cli.plugin.internal;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

public class IsolatedThreadGroup extends ThreadGroup {

    private final Object monitor = new Object();
    private final Log log;

    private Throwable exception;

    public IsolatedThreadGroup(String name, Log log) {
        super(name);
        this.log = log;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        if (!(ex instanceof ThreadDeath)) {
            synchronized (this.monitor) {
                this.exception = (this.exception != null) ? this.exception : ex;
            }
            log.warn(ex);
        }
    }

    public void rethrowUncaughtException() throws MojoExecutionException {
        synchronized (this.monitor) {
            if (this.exception != null) {
                throw new MojoExecutionException("An exception occurred while running. " + this.exception.getMessage(), this.exception);
            }
        }
    }


}
